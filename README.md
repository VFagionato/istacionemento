## Para inicializar o projeto faça os seguintes passos:

---

#### Não esqueça de criar e configurar seu arquivo .env

---

```
git clone git@gitlab.com:davi_coffee/istacionemento.git istacionamento
```

```
cd istacionamento
```

```
pip install -r requirements.txt
```

```
flask db init
```

```
flask db migrate -m "Start table"
```

```
flask db upgrade
```

```
flask run
```
